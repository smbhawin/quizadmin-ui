import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse,
  HttpHandler,
  HttpEvent
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import {AppUiService} from "./app-ui.service";

@Injectable()
export class AppHttpIntercepter implements HttpInterceptor {

  constructor(public appUiService:AppUiService){

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('processing request', request);

    const customReq = request.clone({
    /*
      setHeaders :{
        'Content-Type': 'application/json',
        'Authorization':'123'
      }*/
     //headers: request.headers.set('Content-Type', 'application/json')


    });

    return next
      .handle(customReq)
      .do((ev: HttpEvent<any>) => {
        if (ev instanceof HttpResponse) {
          console.log('processing response', ev);
        }
      })
      .catch(response => {
        if (response instanceof HttpErrorResponse) {
          console.log('Processing http error', response);
          this.appUiService.showErrorTypeToast(response.status.toString(), response.error.data);
        }

        return Observable.throw(response);
      });
  }
}
