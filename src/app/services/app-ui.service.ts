import { Injectable } from '@angular/core';

import {ToasterService, ToasterConfig, Toast} from 'angular2-toaster/angular2-toaster';

export const SweetAlert = require('sweetalert');

@Injectable()
export class AppUiService {

  defaultGameQuestionsCount = 12;



  constructor() { }




  currencies = {
    "INR":{icon:'fa-inr'},
    "USD":{icon:'fa-dollar'},
    "EUR":{icon:'fa-eur'},
    "GBP":{icon:'fa-gbp'}
  }

  defaultCurrency = this.currencies["INR"];

  /**
   * Questions Page
   */

  defaultOptionsCount = 3;
  maximumOptions = 4;


  /**
   * Quick Trivia Questions Page
   */

  qt_defaultOptionsCount = 4;


  /***
   *
   * Color Picker
   */
  pickerColors = [
    '#33cccc',
    '#507255',
    '#DC7F9B',
    '#fabf8f',
    '#bfbfbf',
    '#6699ff',
    '#ff6666',
    '#593D3B',
    '#482646',
    '#72728F',
    '#D36A48',
    '#F26419',
    '#F6AE2D',
    '#3D315B',
    '#9BC53D',
    '#00A6A6',
    '#0B3C49',
    '#232528',
    '#545E75'
  ];
  pickerConfig = {
    width: 25,
    height: 25,
    borderRadius: 13,
    marginToCenter:-4
  };

  pickerDefaultColor: '#EEEEEE';


  /***
   * /////// TOASTER
   */
  toastService:ToasterService;

  toasterConfig: ToasterConfig = new ToasterConfig({
    positionClass: 'toast-top-center',
    showCloseButton: false,
    timeout: 1500
  });


  // TOSATER METHOD
  showSuccessTypeToast(type:String,operation:String) {
    var toaster = {
      type: 'success',
      text: type+' '+operation+ ' Successfully.'
    };
    this.toastService.pop(toaster.type, null, toaster.text);
  }
  showToast(toaster) {
    this.toastService.pop(toaster.type, toaster.title, toaster.text);
  }

  showErrorTypeToast(code:String, message:String) {
    var toaster: Toast = {
      type: 'error',
      body: code+': '+message,
      showCloseButton: true,
      timeout: 3000
    };

    this.toastService.pop(toaster);
  }

  ////////////////////////////////////////////////



}
