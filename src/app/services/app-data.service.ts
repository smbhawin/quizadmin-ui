import {Injectable, OnInit} from '@angular/core';
import {QuestionsService} from "./LiveTrivia/questions/questions.service";
import {CategoriesService} from "./LiveTrivia/categories/categories.service";
import {LevelsService} from "./LiveTrivia/levels/levels.service";
import {GamesService} from "./LiveTrivia/games/games.service";
import {QT_QuestionsService} from './QuickTrivia/questions/questions.service';
import {QT_CategoriesService} from './QuickTrivia/categories/categories.service';
import {QT_LevelsService} from './QuickTrivia/levels/levels.service';
import {QT_GamesService} from './QuickTrivia/games/games.service';

@Injectable()
export class AppDataService implements OnInit{

  constructor(private questionsService:QuestionsService, private categoriesService:CategoriesService, private levelsService:LevelsService, private gamesService:GamesService,
              private qt_questionsService:QT_QuestionsService, private qt_categoriesService:QT_CategoriesService, private qt_levelsService:QT_LevelsService, private qt_gamesService:QT_GamesService) { }


  public questions = [];
  public categories = [];
  public levels = [];
  public games = [];
  private currentGame = null;

  public qt_questions = [];
  public qt_categories = [];
  public qt_levels = [];
  public qt_games = [];

  ngOnInit(){
    this.refreshAll();
  }


  ///////// GAME PLAY LOCAL STORAGE /////////////

  setCurrentGame(_currentGame){
    this.currentGame = _currentGame;
    window.localStorage.setItem('currentGame', JSON.stringify(this.currentGame));
  }
  getCurrentGame(){
    if(!this.currentGame){
      this.currentGame = JSON.parse(window.localStorage.getItem('currentGame'));
    }
    return this.currentGame;
  }
  deleteCurrentGame(){
    this.currentGame = null;
    window.localStorage.removeItem('currentGame');
  }


  ///////////////////////////////////////////



  refreshAll(){
    this.loadLevels()
    this.loadCategories();
    this.loadQuestions();
    this.loadGames();

    this.loadQT_Levels()
    this.loadQT_Categories();
    this.loadQT_Questions();
    this.loadQT_Games();
  }


  loadQuestions(){
    this.questionsService.getQuestions().subscribe(
      (res)=>{
        this.questions = res['data'];
      }
    )
  }

  loadCategories(){
    this.categoriesService.getCategories().subscribe(
      (res)=>{
        this.categories = res['data'];
      }
    )
  }
  loadLevels(){
    this.levelsService.getLevels().subscribe(
      (res)=>{
        this.levels = res['data'];
      }
    )
  }

  loadGames(){
    this.gamesService.getGames().subscribe(
      (res)=>{
        this.games = res['data'];
      }
    )
  }



  loadQT_Questions(){
    this.qt_questionsService.getQuestions().subscribe(
      (res)=>{
        this.qt_questions = res['data'];
      }
    )
  }

  loadQT_Categories(){
    this.qt_categoriesService.getCategories().subscribe(
      (res)=>{
        this.qt_categories = res['data'];
      }
    )
  }
  loadQT_Levels(){
    this.qt_levelsService.getLevels().subscribe(
      (res)=>{
        this.qt_levels = res['data'];
      }
    )
  }

  loadQT_Games(){
    this.qt_gamesService.getGames().subscribe(
      (res)=>{
        this.qt_games = res['data'];
      }
    )
  }







}
