import { Injectable } from '@angular/core';

@Injectable()
export class ConstService {

  constructor() { }

}
export const CLOUD_BUCKET_URL = "https://storage.googleapis.com"

export const DEFAULT_STREAM_V_URL="http://localhost:8000/videostream";
export const DEFAULT_STREAM_A_URL="http://localhost:8000/audiostream";
export const DEFAULT_WEBSOCKET_URL="http://localhost:8000/livegameconnection";


export const WS_HOST = "192.168.1.253";
export const WS_PORT = 8000;

export const GAME_CLIENT_API_URL = "http://192.168.1.253:8369";
export const GET_ANSWER_COUNT = GAME_CLIENT_API_URL+"/min-ans-count";
export const GET_ACTIVE_USERS = GAME_CLIENT_API_URL+"/min-activeusers";
export const SET_GAME_CONFIG_API_URL = GAME_CLIENT_API_URL+"/min-ls";
export const LIVE_STATUS_API_URL = GAME_CLIENT_API_URL+"/livestatus";
export const SET_LIVE_ANSWER_API_URL = GAME_CLIENT_API_URL+"/min-liveanswer";
export const CALCULATE_PRICE =  GAME_CLIENT_API_URL+"/min-calculate-price";
export const LIVE_GAME_CONFIG =  GAME_CLIENT_API_URL+"/min-livegameconfig";
export const UPDATE_PRICE =  GAME_CLIENT_API_URL+"/min-update-price";


export const TRIVIA_API_URL = "http://192.168.1.253:8360";
export const LEVELS_API_URL = TRIVIA_API_URL + "/levels";
export const CATEGORIES_API_URL = TRIVIA_API_URL + "/categories";
export const QUESTIONS_API_URL = TRIVIA_API_URL + "/questions";
export const USERS_API_URL = TRIVIA_API_URL + "/users";
export const GAMES_API_URL = TRIVIA_API_URL + "/games";



export const QT_LEVELS_API_URL = TRIVIA_API_URL + "/quick-trivia/levels";
export const QT_CATEGORIES_API_URL = TRIVIA_API_URL + "/quick-trivia/categories";
export const QT_QUESTIONS_API_URL = TRIVIA_API_URL + "/quick-trivia/questions";
export const QT_GAMES_API_URL = TRIVIA_API_URL + "/quick-trivia/games";



export const IMAGE_NOT_AVAILABLE = CLOUD_BUCKET_URL+'/ij-trivia/imageNotAvailable.png'


/*** Validator Pattern **/

export const EMAIL_VALIDATION = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const CHARS_ONLY =/^[a-zA-Z\-\_]+$/;

export const CHARS_NUM_ONLY =/^[a-zA-Z0-9\-\_]+$/;

/***
  RESPONSE STATUS
 **/

export const SUCCESS = "success";
export const FAILURE = "failure";



/***
 Call endopint types
 **/
export const LEVEL = "Level";
export const CATEGORY = "Category";
export const QUESTION = "Question";
export const USER = "User";
export const GAME = "Game";

/***
 Operations
 **/
export const CREATED = "Created";
export const DELETED = "Deleted";
export const UPDATED = "Updated";

/**
 * Question Types
 */
export const Q_TYPE_SINGLE = "single";
export const Q_TYPE_MULTIPLE = "multiple";
export const Q_TYPE_SEQUENCE = "sequence";

/**
 * Question UI State
 */

export const Q_UI_EDIT = 'Edit';
export const Q_UI_ADD = 'Add';


/**
 * Game UI State
 */

export const G_UI_EDIT = 'Edit';
export const G_UI_ARCHIVED = 'Archived';
export const G_UI_ADD = 'Add New';
