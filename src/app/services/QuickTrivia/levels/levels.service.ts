import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {QT_LEVELS_API_URL} from "../../const.service";

@Injectable()
export class QT_LevelsService {

  constructor(private http:HttpClient) { }

  getLevels(){
    return this.http.get(QT_LEVELS_API_URL);
  }
  getLevelsById(id){
    return this.http.get(QT_LEVELS_API_URL+'/'+id);
  }

  createLevel(level){
    return this.http.post(QT_LEVELS_API_URL,level);
  }

  modifyLevel(level,id){
    return this.http.put(QT_LEVELS_API_URL+'/'+id,level);
  }

  deleteLevel(id){
    return this.http.delete(QT_LEVELS_API_URL+'/'+id);
  }



}
