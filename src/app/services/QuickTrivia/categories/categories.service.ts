import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {QT_CATEGORIES_API_URL} from "../../const.service";

@Injectable()
export class QT_CategoriesService {

  constructor(private http:HttpClient) { }

  getCategories(){
    return this.http.get(QT_CATEGORIES_API_URL);
  }
  getCategoriesById(id){
    return this.http.get(QT_CATEGORIES_API_URL+'/'+id);
  }

  createCatagory(category){
    return this.http.post(QT_CATEGORIES_API_URL,category);
  }

  modifyCategory(category,id){
    return this.http.put(QT_CATEGORIES_API_URL+'/'+id,category);
  }

  deleteCatagory(id){
    return this.http.delete(QT_CATEGORIES_API_URL+'/'+id);
  }

}
