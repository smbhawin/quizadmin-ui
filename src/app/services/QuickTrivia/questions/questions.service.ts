import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {QT_QUESTIONS_API_URL} from '../../const.service';

@Injectable()
export class QT_QuestionsService {

  constructor(private http:HttpClient) { }

  getQuestions(){
    return this.http.get(QT_QUESTIONS_API_URL);
  }
  getArchivedQuestions(){
    return this.http.get(QT_QUESTIONS_API_URL,{params: new HttpParams().set('isArchived','true')});
  }
  getQuestionsById(id){
    return this.http.get(QT_QUESTIONS_API_URL+'/'+id);
  }

  createQuestion(question){
    return this.http.post(QT_QUESTIONS_API_URL,question);
  }

  modifyQuestion(question,id){
    return this.http.put(QT_QUESTIONS_API_URL+'/'+id,question);
  }

  deleteQuestion(id){
    return this.http.delete(QT_QUESTIONS_API_URL+'/'+id);
  }

  archiveQuestion(isArchived,id){
    var archive = {
      //id:id.toString(),
      isArchived:isArchived.toString()
    };
    return this.http.post(QT_QUESTIONS_API_URL+'/'+id+'/archive',archive);
  }
  setPrice(price,id){
    var priceObj = {
      price:price.toString()
    };
    return this.http.post(QT_QUESTIONS_API_URL+'/'+id+'/price',priceObj);
  }
}
