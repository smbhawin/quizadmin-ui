import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {
  CALCULATE_PRICE,
  QT_GAMES_API_URL, GET_ACTIVE_USERS, GET_ANSWER_COUNT, LIVE_GAME_CONFIG, LIVE_STATUS_API_URL, SET_GAME_CONFIG_API_URL,
  SET_LIVE_ANSWER_API_URL, UPDATE_PRICE
} from '../../const.service';
import {HttpParams} from "@angular/common/http";

@Injectable()
export class QT_GamesService {

  constructor(private http:HttpClient) { }

  getGames(){
    return this.http.get(QT_GAMES_API_URL);
  }
  getArchivedGames(){
    return this.http.get(QT_GAMES_API_URL,{params: new HttpParams().set('isArchived','true')});
  }
  getGamesById(id){
    return this.http.get(QT_GAMES_API_URL+'/'+id);
  }

  createGame(game){
    return this.http.post(QT_GAMES_API_URL,game);
  }

  modifyGame(game,id){
    return this.http.put(QT_GAMES_API_URL+'/'+id,game);
  }

  deleteGame(id){
    return this.http.delete(QT_GAMES_API_URL+'/'+id);
  }

  archiveGame(isArchived,id){
    var archive = {
      isArchived:isArchived.toString()
    };
    return this.http.post(QT_GAMES_API_URL+'/'+id+'/archive',archive);
  }





}
