import { Injectable } from '@angular/core';
import {FormControl} from "@angular/forms";

@Injectable()
export class ValidationHelper {

  constructor() { }

  public static noWhitespace(control: FormControl) {
    let isWhitespace = (control.value || '').trim().length === 0;
    let isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true }
  }

}
