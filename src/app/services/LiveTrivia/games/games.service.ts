import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {
  CALCULATE_PRICE,
  GAMES_API_URL, GET_ACTIVE_USERS, GET_ANSWER_COUNT, LIVE_GAME_CONFIG, LIVE_STATUS_API_URL, SET_GAME_CONFIG_API_URL,
  SET_LIVE_ANSWER_API_URL, UPDATE_PRICE
} from '../../const.service';
import {HttpParams} from "@angular/common/http";

@Injectable()
export class GamesService {

  constructor(private http:HttpClient) { }

  getGames(){
    return this.http.get(GAMES_API_URL);
  }
  getArchivedGames(){
    return this.http.get(GAMES_API_URL,{params: new HttpParams().set('isArchived','true')});
  }
  getGamesById(id){
    return this.http.get(GAMES_API_URL+'/'+id);
  }

  createGame(game){
    return this.http.post(GAMES_API_URL,game);
  }

  modifyGame(game,id){
    return this.http.put(GAMES_API_URL+'/'+id,game);
  }

  deleteGame(id){
    return this.http.delete(GAMES_API_URL+'/'+id);
  }

  archiveGame(isArchived,id){
    var archive = {
      isArchived:isArchived.toString()
    };
    return this.http.post(GAMES_API_URL+'/'+id+'/archive',archive);
  }

  getAnswerCount(game_id,question_id,oid){
    return this.http.get(GET_ANSWER_COUNT+'/'+game_id+'/'+question_id+'/'+oid);
  }

  getActiveUsers(game_id,limit){
    return this.http.get(GET_ACTIVE_USERS+'/'+game_id+'/'+limit);
  }


  setGameConfig(config){
    return this.http.post(SET_GAME_CONFIG_API_URL,config);
  }

  getGameConfig(){
    return this.http.get(LIVE_STATUS_API_URL);
  }


  setLiveAnswer(liveAnswer){
    return this.http.post(SET_LIVE_ANSWER_API_URL,liveAnswer);
  }

  calculatePrice(game_id, question_id){
    return this.http.get(CALCULATE_PRICE+'/'+game_id+'/'+question_id);
  }

  setLiveGameConfig(config){
    return this.http.post(LIVE_GAME_CONFIG,config);
  }


  updatePrice(game_id){
    return this.http.get(UPDATE_PRICE+'/'+game_id);
  }




}
