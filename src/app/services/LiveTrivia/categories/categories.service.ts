import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CATEGORIES_API_URL} from "../../const.service";

@Injectable()
export class CategoriesService {

  constructor(private http:HttpClient) { }

  getCategories(){
    return this.http.get(CATEGORIES_API_URL);
  }
  getCategoriesById(id){
    return this.http.get(CATEGORIES_API_URL+'/'+id);
  }

  createCatagory(category){
    return this.http.post(CATEGORIES_API_URL,category);
  }

  modifyCategory(category,id){
    return this.http.put(CATEGORIES_API_URL+'/'+id,category);
  }

  deleteCatagory(id){
    return this.http.delete(CATEGORIES_API_URL+'/'+id);
  }

}
