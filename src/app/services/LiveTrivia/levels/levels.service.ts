import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {LEVELS_API_URL} from "../../const.service";

@Injectable()
export class LevelsService {

  constructor(private http:HttpClient) { }

  getLevels(){
    return this.http.get(LEVELS_API_URL);
  }
  getLevelsById(id){
    return this.http.get(LEVELS_API_URL+'/'+id);
  }

  createLevel(level){
    return this.http.post(LEVELS_API_URL,level);
  }

  modifyLevel(level,id){
    return this.http.put(LEVELS_API_URL+'/'+id,level);
  }

  deleteLevel(id){
    return this.http.delete(LEVELS_API_URL+'/'+id);
  }



}
