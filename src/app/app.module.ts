import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';

import { CoreModule } from './core/core.module';
import { LayoutModule } from './layout/layout.module';
import { SharedModule } from './shared/shared.module';
import { RoutesModule } from './routes/routes.module';
import {AppHttpIntercepter} from "./services/app-http-intercepter.service";
import {ConstService} from "./services/const.service";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {LevelsService} from "./services/LiveTrivia/levels/levels.service";
import {CategoriesService} from "./services/LiveTrivia/categories/categories.service";
import {ValidationHelper} from "./services/validation-helper.service";
import {AppUiService} from "./services/app-ui.service";
import {QuestionsService} from "./services/LiveTrivia/questions/questions.service";
import {AppDataService} from "./services/app-data.service";
import {GamesService} from "./services/LiveTrivia/games/games.service";
import {QT_CategoriesService} from './services/QuickTrivia/categories/categories.service';
import {QT_LevelsService} from './services/QuickTrivia/levels/levels.service';
import {QT_GamesService} from './services/QuickTrivia/games/games.service';
import {QT_QuestionsService} from './services/QuickTrivia/questions/questions.service';
// https://github.com/ocombe/ng2-translate/issues/218
export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        HttpClientModule,
        BrowserAnimationsModule, // required for ng2-tag-input
        CoreModule,
        LayoutModule,
        SharedModule.forRoot(),
        RoutesModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (createTranslateLoader),
                deps: [HttpClient]
            }
        })
    ],
    providers: [
      ConstService,
      LevelsService,
      CategoriesService,
      GamesService,
      QuestionsService,
      QT_CategoriesService,
      QT_LevelsService,
      QT_GamesService,
      QT_QuestionsService,
      ValidationHelper,
      AppUiService,
      AppDataService,
      { provide: HTTP_INTERCEPTORS, useClass: AppHttpIntercepter, multi: true }
      ],
    bootstrap: [AppComponent]
})
export class AppModule { }
