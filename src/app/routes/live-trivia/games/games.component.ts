import {Component, OnInit, ViewChild, ApplicationRef, Injector} from '@angular/core';
import {DatatableComponent} from "@swimlane/ngx-datatable";
import {ModalDirective} from "ngx-bootstrap";
import {AppDataService} from "../../../services/app-data.service";
import {AppUiService} from "../../../services/app-ui.service";
import {QuestionsService} from "../../../services/LiveTrivia/questions/questions.service";
import {FormGroup, Validators, FormBuilder} from "@angular/forms";
import {G_UI_ADD, GAME, CREATED, UPDATED, G_UI_EDIT, G_UI_ARCHIVED} from "../../../services/const.service";
import {GamesService} from "../../../services/LiveTrivia/games/games.service";
import {NumberPickerComponent} from "../components/number-picker/number-picker.component";
import * as moment from 'moment-timezone';
import {Router} from "@angular/router";

const _clone = (d) => JSON.parse(JSON.stringify(d));

@Component({
  selector: 'app-games',
  templateUrl: './games.component.html',
  styleUrls: ['./games.component.scss']
})
export class GamesComponent implements OnInit {
  router: Router;
  gameForm:FormGroup;
  temp = [];
  rows = [];
  games = [];
  archivedGames = [];
  rowsFilter = [];
  selected = [];
  gameQuestionsCount = this.appUiService.defaultGameQuestionsCount;
  currentGQindex:number = -1;
  currentSelection = {};
  gameGridSelected = [];
  gameArchivedGridSelected = [];
  gameMode = G_UI_ADD;
  payLoad = ''
  isQuestionSelectionError = false;

  @ViewChild(DatatableComponent) table: DatatableComponent;
  @ViewChild('gamesGrid') gamesGrid: DatatableComponent;
  @ViewChild('gamesArchivedGrid') gamesArchivedGrid: DatatableComponent;


  @ViewChild('questionModal') questionModal: ModalDirective;
  @ViewChild('queCtrComp') queCtrComp: NumberPickerComponent;


  constructor(private questionsService:QuestionsService,
              public appUiService:AppUiService,
              public formBuilder:FormBuilder,
              public appDataService:AppDataService,
              public gamesService:GamesService,
              public injector: Injector) { }

  ngOnInit() {
    this.router = this.injector.get(Router);
    this.appDataService.refreshAll();
    this.loadQuestions();
    this.loadGames()
    this.loadArchivedGames();
    this.initGamesForm(null);
  }

  ngAfterViewInit(){

  }

  loadGames(){
    this.gamesService.getGames().subscribe(
      (res)=>{
        console.log(res)
        this.games = _clone(res['data']);
        this.appDataService.games = res['data'];
      }
    )
  }

  loadArchivedGames(){
    var sq = {"isArchived":true};

    this.gamesService.getArchivedGames().subscribe(
      (res)=>{
        console.log(res)
        this.archivedGames = _clone(res['data']);
      }
    )
  }


  initGamesForm(game){

    if(!game){
      game = {};
      game.game_name = '';
      game.start_date_time = '';
      game.price = '';
      game.questions = [];
      for( var i=0;i<this.gameQuestionsCount;i++){
        game.questions.push({});
      }
    }else{
      this.gameQuestionsCount = game.questions.length;
      this.queCtrComp.setValue(this.gameQuestionsCount);
    }
    this.currentSelection = game;


    this.gameForm = this.formBuilder.group({
      game_name:[{value:game.game_name, disabled: (this.gameMode == G_UI_ARCHIVED)},Validators.compose([Validators.required])],
      start_date_time:[game.start_date_time,Validators.compose([Validators.required])],
      price:[game.price,Validators.compose([Validators.required])]
    })


    /*
      if(!question.options){
        this.generateOptions(null,this.appUiService.defaultOptionsCount)
      }else{
        this.generateOptions(question.options,question.options.length)
        setTimeout( this.selectFieldsOnEdit,500,this);
      }
    */

  }


  setPrice(question){
    if(question.isDirty){
      this.questionsService.setPrice(question.qPrice,question.question_id).subscribe(
        (res)=>{
          if( res['status'] == 'success'){
            question.isDirty = false;
            console.log(res);
          }
        }
      )
    }
  }



  createOrModifyGame(){


    ///////////// validate question
    for(var i=0;i<this.currentSelection['questions'].length;i++){
      if(!this.currentSelection['questions'][i]._id){
        this.isQuestionSelectionError = true;
        return;
      }
    }

    ///////////


    var questions = [];
    for(var i=0;i<this.currentSelection['questions'].length;i++){
      questions.push(this.currentSelection['questions'][i]._id)
    }
    ///////////////////////////////////////////////////////////////////////////




    var game = this.gameForm.value;
    game.questions = questions;


    this.payLoad = JSON.stringify(game);


    if(this.gameMode == G_UI_ADD){
      this.gamesService.createGame(game).subscribe(
        (res)=>{
          console.log(res)
          this.appUiService.showSuccessTypeToast(GAME,CREATED);
          this.resetAll();
        }
      )
    }else{
      this.gamesService.modifyGame(game,this.currentSelection['game_id']).subscribe(
        (res)=>{
          console.log(res)
          this.appUiService.showSuccessTypeToast(GAME,UPDATED);
          this.resetAll();
        }

      )
    }


  }

  resetAll(){
    this.gameForm.reset();
    this.currentGQindex = -1;
    this.currentSelection = {};
    this.gameGridSelected = [];
    this.selected = [];
    this.gameMode = G_UI_ADD;
    this.isQuestionSelectionError = false;
    this.payLoad = '';
    this.loadGames();
    this.loadArchivedGames();
    this.loadQuestions();
    this.initGamesForm(null);
    this.gameQuestionsCount = this.appUiService.defaultGameQuestionsCount;
    this.queCtrComp.setValue(this.gameQuestionsCount);

  }


  loadQuestions(){
    this.questionsService.getQuestions().subscribe(
      (res)=>{
        console.log(res)
        this.temp = _clone(res['data']);
        this.rows = _clone(res['data']);
        this.rowsFilter = _clone(res['data']);
      }
    )
  }


  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.question.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rowsFilter = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  // Selection


  filterBy(field,value){
    value = value.toLowerCase();
    // filter our data
    const temp = this.temp.filter(function(d) {

      var flds =  field.split('.');
      var dfld = null;

      for(var i=0;i<flds.length;i++){
        dfld = (dfld)?dfld = dfld[flds[i]]:d[flds[i]];
      }

      return dfld.toLowerCase().indexOf(value) !== -1 || !value;
    });

    // update the rows
    this.rowsFilter = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  //TODO: Implement filter on selected questions
  filterSelectedQuestions(){

    var temp = this.temp;

    for(var i=0;i<this.currentSelection['questions'].length;i++){
     // temp.splice(temp.indexOf(this.currentSelection['questions'][i]),1);

      temp = this.removeElement(temp, this.currentSelection['questions'][i]);
    }

    // update the rows
    this.rowsFilter = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;

  }

  removeElement(array, element) {
    return array.filter(e => e !== element);
  }



  onSelectQuestion({ selected }) {
    this.selected = selected
  }

  selectGameQuestion(){

    this.currentSelection['questions'][this.currentGQindex] = this.selected[0];
    this.currentGQindex = -1;
    this.selected = [];
    this.closeQuestionModal();
  }

  addEmptyGameQuestion(){
    if(this.gameMode == G_UI_ARCHIVED){
      return;
    }
    this.currentSelection["questions"].push({});
    this.gameQuestionsCount = this.currentSelection['questions'].length;
    this.queCtrComp.setValue(this.gameQuestionsCount);
  }
  removeLastGameQuestion(){
    if(this.gameMode == G_UI_ARCHIVED){
      return;
    }
    this.currentSelection["questions"].pop();
    this.gameQuestionsCount = this.currentSelection['questions'].length;
    this.queCtrComp.setValue(this.gameQuestionsCount);
  }

  clearGameQuestion(GQindex){
    if(this.gameMode == G_UI_ARCHIVED){
      return;
    }

    this.questionsService.archiveQuestion(false,this.currentSelection['questions'][GQindex]['question_id']).subscribe(
      (res)=> {
        console.log(res);
        this.currentSelection['questions'][GQindex] = {};

      }
    )
  }


  putGameQuestion(GQindex){
    //TODO: Implement filter on selected questions
   // this.filterSelectedQuestions();
    this.openQuestionModal();
    this.currentGQindex = GQindex;

    setTimeout( function(this$){
      var level_name = ((GQindex+1).toString().length == 1)?'0'+(GQindex+1):(GQindex+1);

      this$.filterBy('level.level_name',level_name.toString());
    },500,this);


  }



  openQuestionModal(){
    this.questionModal.show()
  }
  closeQuestionModal()
  {
    this.questionModal.hide()
  }


  onQuestionCountChanged(e){
    console.log(e);
    if(e>this.gameQuestionsCount){
      this.addEmptyGameQuestion();
    }else if(e<this.gameQuestionsCount){
      this.removeLastGameQuestion();
    }
  }


  onSelectGame({ selected },isArchived = false) {

    if(isArchived){
      this.gameMode = G_UI_ARCHIVED;
    }else{
      this.gameMode = G_UI_EDIT;
    }
    this.gameGridSelected = selected;

    this.initGamesForm(selected[0]);

  }



  gamePlay(){
    this.appDataService.setCurrentGame(this.gameGridSelected[0]);
    this.router.navigate(['/trivia/game-play']);
  }



  archiveGame(isArchived = true){
    this.gamesService.archiveGame(isArchived,this.currentSelection['game_id']).subscribe(
      (res)=> {
        console.log(res);
        this.resetAll()
      }
    )
  }

  formateGameDate(dt){
    return moment(dt).tz('America/New_York').format( 'ddd, MM/D/YYYY, h:mm A z');
  }


  print(): void {
    let printContents, popupWin;
    printContents = document.getElementById('print-section').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title></title>
          <style>
           body{
  font-family: "Helvetica Neue", Arial, sans-serif;
}

.center {
  text-align: center;
}
.right{
  text-align: right;
}
.question{
  font-size: medium;
  font-weight: bold;
}
.options{
  .correct{
    font-weight: bold;
    text-decoration: underline;
  }
}
tr{
  border: 1px #777777 solid;
}
          </style>
        </head>
    <body onload="window.print();">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}
