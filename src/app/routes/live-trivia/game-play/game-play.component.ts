import * as socketCluster from "socketcluster-client";
import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {SettingsService} from "../../../core/settings/settings.service";
import {AppDataService} from "../../../services/app-data.service";
import * as moment from 'moment-timezone';
import {ThemesService} from "../../../core/themes/themes.service";
import {document} from "ngx-bootstrap/utils/facade/browser";
import {
  CHARS_ONLY, DEFAULT_STREAM_A_URL, DEFAULT_STREAM_V_URL, DEFAULT_WEBSOCKET_URL, WS_HOST,
  WS_PORT
} from '../../../services/const.service';
import {GamesService} from '../../../services/LiveTrivia/games/games.service';
import {ModalDirective} from 'ngx-bootstrap';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

export const SEND_QUESTION = 'lapregunta';
export const SEND_ANSWER = 'larazon';
export const SEND_WINNERS = 'leganador';


@Component({
  selector: 'app-game-play',
  templateUrl: './game-play.component.html',
  styleUrls: ['./game-play.component.scss']
})

export class GamePlayComponent implements OnInit, OnDestroy {


  @ViewChild('gameSettingsModal') gameSettingsModal: ModalDirective;
  CLOUD_BUCKET_URL = "https://storage.googleapis.com";
  clientSocket;
  liveUsersCount;
  currentGame;
  currentQuestionId = 1;
  optionCount= {};
  gameSettingsForm:FormGroup;
  isLive = false;
  isAnswerActive = false;
  isAnserSent = false;
  isQuestionSent = false;
  isLast = false;
  winnerUsers = {
    count:0,
    price:0
  };

  activeUsersList = [];

  glc; // Game Live

  socketOptions = {
    hostname: WS_HOST,
    port: WS_PORT
  };

  constructor(public settings: SettingsService, public appDataService:AppDataService, public themesService:ThemesService, public gamesService:GamesService, public formBuilder:FormBuilder) {


  }

  ngOnInit() {
    setInterval(function($this){
      $this.settings.layout.isCollapsed = true;
      $this.themesService.setTheme('G')
    },100,this)
    this.currentGame = this.appDataService.getCurrentGame();
    document.body.classList.add("dark-background");




    this.gameSettingsForm = this.formBuilder.group({
      game_status:['OFFLINE'],
      streamV: [DEFAULT_STREAM_V_URL],
      streamA: [DEFAULT_STREAM_A_URL],
      plug: [DEFAULT_WEBSOCKET_URL],
      start_date_time:[new Date(),Validators.compose([Validators.required])],
      price:['',Validators.compose([Validators.required])]


    });

  }

  ngOnDestroy() {
    document.body.classList.remove("dark-background");
    this.themesService.setTheme('C');
    this.settings.layout.isCollapsed = false;
  }



  connectToSocket(){


    this.clientSocket = socketCluster.create(this.socketOptions);

    this.clientSocket.on('connect',
      function(){
        console.log('client connect');

        this.glc = this.clientSocket.subscribe('game-live');

        this.glc.watch(function(data){

          if( data.type === 'user-count'){
            this.liveUsersCount = data.clientsCount;
          }
          console.log(data);
        }.bind(this));






      }.bind(this)
    );
    this.clientSocket.on('event', function(data){
      console.log(data);
    });
    this.clientSocket.on('disconnect', function(){
      console.log('client disconnected');
    });

    this.clientSocket.on('connect_error', (error) => {
      console.log('client connection error' + error);
    });
    this.clientSocket.on('connect_timeout', (timeout) => {
      console.log('client connect_timeout' + timeout);
    });
    this.clientSocket.on('error', (error) => {
      console.log('client Error ' + error);
    });




    this.clientSocket.on('user-count', function(count){
      this.liveUsersCount = count;
    }.bind(this));





  }
  getIST(dt = new Date()){
    return moment(dt).tz('Asia/Kolkata').format( 'ddd, MM/DD, hh:mm:ss A');
  }
  timeRemaining(){
    let ms = moment().diff(this.currentGame.start_date_time);
    let mDuration = moment.duration(0-ms);

    if(this.isLive){
      mDuration = moment.duration(ms);
    }

    return mDuration;
    //return mDuration.days()+'d '+ mDuration.hours()+'h'+  mDuration.minutes()+'m '+  mDuration.seconds()+'s';
  }






  goToNextQuestion(){
    if(this.currentQuestionId < this.currentGame.questions.length){
      this.currentQuestionId++;
      this.resetOnNext();
    }
  }
  goToPreviousQuestion(){
    if(this.currentQuestionId>1 && this.currentQuestionId <= this.currentGame.questions.length){
      this.currentQuestionId--;
    }
  }


  sendQuestion() {

    var thisQuestion = this.currentGame.questions[this.currentQuestionId-1];

    this.isLast = (this.currentQuestionId == this.currentGame.questions.length);


    this.setLiveAnswer(thisQuestion.options,true, this.isLast);



    var thisOptions = [];

    for(var i=0;i<thisQuestion.options.length;i++){
      thisOptions.push({
        title:thisQuestion.options[i].title,
        oid:thisQuestion.options[i].oid,
      })
    }

    var leQuestion = {
      qid: thisQuestion._id,
      question: thisQuestion.question,
      options:thisOptions,
      wo: thisQuestion.wrong_option
    }

    this.clientSocket.emit(SEND_QUESTION,leQuestion);

    setTimeout(function($this,leQuestion){

      $this.setLiveAnswer({},false);


      for(var i=0;i<leQuestion.options.length;i++){
        $this.getAnswerCount($this.currentGame.game_id, leQuestion.qid, leQuestion.options[i].oid);
      }


      $this.getActiveUsers($this.currentGame.game_id, ($this.isLast) ? 0 : 20);


      $this.isAnserSent = false;
      $this.isAnswerActive = true;

    },19000, this, leQuestion);


    this.isQuestionSent = true;


  }


  setLiveAnswer(options, answer_mode = false,is_last = false) {
    if(!answer_mode){

      this.gamesService.setLiveAnswer({correct_oid: '', answer_mode: false, is_last: false}).subscribe(
        (res) => {
          console.log(res['data']);
        }
      );

    }else{
      for( var i = 0; i < options.length; i++) {
        if(options[i].isCorrect) {
          var liveAnswer = {correct_oid: options[i].oid, answer_mode: true, is_last: is_last}

          this.gamesService.setLiveAnswer(liveAnswer).subscribe(
            (res) => {
              console.log(res['data']);
            }
          );
          break;
        }

      }
    }
  }



  sendAnswer(){

    var thisAnswer = this.currentGame.questions[this.currentQuestionId-1];


    var thisOptions = [];

    for(var i=0;i<thisAnswer.options.length;i++){
      thisOptions.push({
        title:thisAnswer.options[i].title,
        oid:thisAnswer.options[i].oid,
        isCorrect: thisAnswer.options[i].isCorrect,
        fbCnt: this.optionCount[thisAnswer.options[i].oid]
      })
    }

    var leAnswer = {
      qid: thisAnswer._id,
      question: thisAnswer.question,
      options: thisOptions
    }

    if(this.currentQuestionId == this.currentGame.questions.length){
      leAnswer['isLast'] = true;
      this.getWinnersCount();
    }

    this.clientSocket.emit(SEND_ANSWER,leAnswer);
    this.isAnserSent = true;

    this.calculatePrice(this.currentGame.game_id, thisAnswer._id);
  }



  getAnswerCount(game_id,question_id,oid){
    this.gamesService.getAnswerCount(game_id,question_id,oid).subscribe(
      (res)=>{
        this.optionCount[res['data'].oid] = res['data'].fbCnt;
        console.log(res['data']);
      }
    );
  }


  getActiveUsers(game_id,limit){

    this.gamesService.getActiveUsers(game_id,limit).subscribe(
      (res)=>{

        res['data'].sort(function(a, b) {
          return  b.game_earning - a.game_earning;
        });

        this.activeUsersList = res['data'];
        console.log(res['data']);
      }
    );
  }

  getWinnersCount(){
    var thisOptions = this.currentGame.questions[this.currentQuestionId-1].options

    for(var i=0;i<thisOptions.length;i++){
      if( thisOptions[i].isCorrect) {
        this.winnerUsers.count = this.optionCount[thisOptions[i].oid];
        this.winnerUsers.price = this.currentGame.price / this.winnerUsers.count;
        break;
      }
    }

  }


  calculatePrice(game_id, question_id){
    this.gamesService.calculatePrice(game_id,question_id).subscribe(
      (res) => {
        if(res['data']) {
          res['data'].sort(function(a, b) {
            return  b.game_earning - a.game_earning;
          });

          this.activeUsersList = res['data'];
        }
        console.log(res['data']);
      }
    );
  }




  sendWinners(){
    var leWinners = (this.activeUsersList.length > 20) ? this.activeUsersList.slice(0, 20) : this.activeUsersList;
    this.clientSocket.emit(SEND_WINNERS, {count: this.winnerUsers.count, wList: leWinners});

    /**
     * Update Price of All Users
     */
    this.gamesService.updatePrice(this.currentGame.game_id).subscribe(
      (res) => {
        console.log(res['data']);
      }
    );
  }




  resetOnNext(){
    this.isAnswerActive = false;
    this.isAnserSent = false;
    this.isQuestionSent = false;
  }

  openGameSettingsModal(){
    this.gameSettingsModal.show();
  }
  closeGameSettingsModal()
  {
    this.gameSettingsModal.hide();
  }


  startGame() {
    this.currentQuestionId = 1;

    var configObj = {
      gun: this.currentGame.game_id,
      game_status: this.gameSettingsForm.value.game_status,
      next_game: {
        price: this.gameSettingsForm.value.price,
        start_time: this.gameSettingsForm.value.start_date_time
      },
      plug: this.gameSettingsForm.value.plug,
      stream: {
        streamV: this.gameSettingsForm.value.streamV,
        streamA: this.gameSettingsForm.value.streamA,
      }
    };

    this.gamesService.setGameConfig(configObj).subscribe(
      (res)=>{
        //this.optionCount[res['data'].oid] = res['data'].fbCnt;
        console.log(res['data']);
        this.currentGame.start_date_time = new Date();
        this.isLive = true;
        this.connectToSocket();
        this.closeGameSettingsModal();
      }
    );


    var liveGameConfigObj = {
      gun: this.currentGame.game_id,
      game_status: this.gameSettingsForm.value.game_status,
      priceUpdated: false
    };

    this.gamesService.setLiveGameConfig(liveGameConfigObj).subscribe(
      (res)=>{
        console.log(res['data']);
      }
    );


  }

  endGame() {

    this.gamesService.getGameConfig().subscribe(
      (res)=>{
        var configObj = {
          game_status: 'OFFLINE',
          next_game: res['data'].cf_value.next_game
        };

        this.gamesService.setGameConfig(configObj).subscribe(
          (res) => {
            this.isLive = false;
            this.resetOnNext();
            console.log(res['data']);
          }
        );

      }
    );
  }



/*

{
  "question": "At which building was the famous Rocky Balboa steps scene filmed?",
  "options": [
    {
      "isCorrect": false,
      "title": "Philadelphia City Hall"
    },
    {
      "isCorrect": false,
      "title": "Independence Hall"
    },
    {
      "isCorrect": true,
      "title": "Phila Museum of Art History"
    }
    ]
}


*/








}
