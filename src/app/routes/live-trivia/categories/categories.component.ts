import { Component, OnInit, ViewChild  } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {CategoriesService} from "../../../services/LiveTrivia/categories/categories.service";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {CHARS_ONLY, CATEGORY, UPDATED, DELETED, CREATED} from "../../../services/const.service";
import {AppUiService} from "../../../services/app-ui.service";
import {AppDataService} from "../../../services/app-data.service";


const _clone = (d) => JSON.parse(JSON.stringify(d));


@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  addCategoryForm:FormGroup;
  editCategoryForm:FormGroup;

  addCategoryColor = '';

  temp = [];
  rows = [];
  rowsFilter = [];
  selected = [];
  currentSelection = {category_id:null,category_name:'',color:this.appUiService.pickerDefaultColor};

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor( private categoriesService:CategoriesService,
               public formBuilder:FormBuilder,
               public appUiService:AppUiService,
               private appDataService:AppDataService) {

      this.addCategoryColor = this.appUiService.pickerDefaultColor;

  }

  ngOnInit() {

    this.loadCategories();

    this.addCategoryForm = this.formBuilder.group({
      category_name:['',Validators.compose([Validators.required,Validators.pattern(CHARS_ONLY)])]
    })
    this.editCategoryForm = this.formBuilder.group({
      category_name:['',Validators.compose([Validators.required,Validators.pattern(CHARS_ONLY)])]
    })

  }



  loadCategories(){
    this.categoriesService.getCategories().subscribe(
      (res)=>{
        console.log(res)

        this.temp = _clone(res['data']);
        this.rows = _clone(res['data']);
        this.rowsFilter = _clone(res['data']);

        this.appDataService.categories = res['data'];
      }
    )
  }


  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.category_name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rowsFilter = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  // Selection


  onSelect({ selected }) {
    this.editCategoryForm.reset();
    this.selected = selected
    this.currentSelection = this.selected[0];
  }



  resetAll() {
    this.loadCategories();
    this.selected = [];
    this.currentSelection = {category_id:null,category_name:'',color:this.appUiService.pickerDefaultColor};
    this.addCategoryColor = this.appUiService.pickerDefaultColor;
    this.addCategoryForm.reset();
    this.editCategoryForm.reset();
  }


  createCatagory(){
    let category = {
      category_name: this.addCategoryForm.controls.category_name.value,
      color: this.addCategoryColor
    }

    this.categoriesService.createCatagory(category).subscribe(
      (res)=>{
        console.log(res)
        this.appUiService.showSuccessTypeToast(CATEGORY,CREATED);
        this.resetAll();
      }
    )

  }

  modifyCategory(){
    let category = {
      category_name: this.editCategoryForm.controls.category_name.value,
      color: this.currentSelection.color
    }

    this.categoriesService.modifyCategory(category,this.currentSelection.category_id).subscribe(
      (res)=>{
        console.log(res)
        this.appUiService.showSuccessTypeToast(CATEGORY,UPDATED);
        this.resetAll();
      }

    )

  }
  deleteCategory(){
    this.categoriesService.deleteCatagory(this.currentSelection.category_id).subscribe(
      (res)=>{
        console.log(res)
        this.appUiService.showSuccessTypeToast(CATEGORY,DELETED);
        this.resetAll();
      }
    )

  }



}
