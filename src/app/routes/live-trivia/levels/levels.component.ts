import { Component, OnInit, ViewChild  } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {LevelsService} from "../../../services/LiveTrivia/levels/levels.service";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {LEVEL, UPDATED, DELETED, CREATED, CHARS_NUM_ONLY} from "../../../services/const.service";
import {AppUiService} from "../../../services/app-ui.service";
import {AppDataService} from "../../../services/app-data.service";


const _clone = (d) => JSON.parse(JSON.stringify(d));


@Component({
  selector: 'app-levels',
  templateUrl: './levels.component.html',
  styleUrls: ['./levels.component.scss']
})
export class LevelsComponent implements OnInit {

  addLevelForm:FormGroup;
  editLevelForm:FormGroup;

  temp = [];
  rows = [];
  rowsFilter = [];
  selected = [];
  currentSelection = {level_id:null,level_name:''};

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor( private levelsService:LevelsService,
               public formBuilder:FormBuilder,
               public appUiService:AppUiService,
               private appDataService:AppDataService) {
  }

  ngOnInit() {

    this.loadLevels();

    this.addLevelForm = this.formBuilder.group({
      level_name:['',Validators.compose([Validators.required,Validators.pattern(CHARS_NUM_ONLY)])]
    })
    this.editLevelForm = this.formBuilder.group({
      level_name:['',Validators.compose([Validators.required,Validators.pattern(CHARS_NUM_ONLY)])]
    })

  }



  loadLevels(){
    this.levelsService.getLevels().subscribe(
      (res)=>{
        console.log(res)

        this.temp = _clone(res['data']);
        this.rows = _clone(res['data']);
        this.rowsFilter = _clone(res['data']);

        this.appDataService.levels = res['data'];

      }
    )
  }


  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.level_name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rowsFilter = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  // Selection


  onSelect({ selected }) {
    this.editLevelForm.reset();
    this.selected = selected
    this.currentSelection = this.selected[0];
  }



  resetAll() {
    this.loadLevels();
    this.selected = [];
    this.currentSelection = {level_id:null,level_name:''};
    this.editLevelForm.reset();
    this.addLevelForm.reset();
  }


  createCatagory(){
    let level = {
      level_name: this.addLevelForm.controls.level_name.value
    }

    this.levelsService.createLevel(level).subscribe(
      (res)=>{
        console.log(res)
        this.appUiService.showSuccessTypeToast(LEVEL,CREATED);
        this.resetAll();
      }
    )

  }

  modifyLevel(){
    let level = {
      level_name: this.editLevelForm.controls.level_name.value
    }

    this.levelsService.modifyLevel(level,this.currentSelection.level_id).subscribe(
      (res)=>{
        console.log(res)
        this.appUiService.showSuccessTypeToast(LEVEL,UPDATED);
        this.resetAll();
      }

    )

  }
  deleteLevel(){
    this.levelsService.deleteLevel(this.currentSelection.level_id).subscribe(
      (res)=>{
        console.log(res)
        this.appUiService.showSuccessTypeToast(LEVEL,DELETED);
        this.resetAll();
      }
    )

  }



}
