import { Component, OnInit, ViewChild  } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import {FormGroup, FormBuilder, Validators, FormArray, FormControl} from "@angular/forms";
import {
  CATEGORY, UPDATED, DELETED, CREATED, Q_TYPE_SEQUENCE,
  QUESTION, Q_TYPE_SINGLE, Q_UI_ADD, Q_UI_EDIT
} from "../../../services/const.service";
import {AppUiService, SweetAlert} from "../../../services/app-ui.service";
import {AppDataService} from "../../../services/app-data.service";
import {ModalDirective} from "ngx-bootstrap";
import {QT_QuestionsService} from '../../../services/QuickTrivia/questions/questions.service';
const uuid = require('uuid');

const _clone = (d) => JSON.parse(JSON.stringify(d));


@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {

  questionForm:FormGroup;
  questionFormDom:HTMLElement;

  questionMode = Q_UI_ADD;

  showArchived = false;

  temp = [];
  rows = [];
  rowsFilter = [];
  selected = [];
  currentSelection;
  addQuestionFromOptions = new FormArray([]);
  isCorrectSelectionError = false;


  payLoad = '';

  @ViewChild(DatatableComponent) table: DatatableComponent;


  @ViewChild('questionModal') questionModal: ModalDirective;


  constructor( private questionsService:QT_QuestionsService,
               public formBuilder:FormBuilder,
               public appUiService:AppUiService,
               public appDataService:AppDataService,
        ) {

  }

  ngOnInit() {
    this.appDataService.refreshAll();


    this.loadQuestions();

    this.initQuestionForm(null);



  }

  initQuestionForm(question){

    this.addQuestionFromOptions = new FormArray([]);


    if(!question){
      question = {};
      question.question = '';
      question.type = Q_TYPE_SINGLE;
      question.category = '';
      question.level = '';
      question.fact = '';
      question.wrong_option = '';
    }



    this.questionForm = this.formBuilder.group({
      question:[question.question,Validators.compose([Validators.required])],
      type:[question.type],
      options:this.addQuestionFromOptions,
      category:[question.category,Validators.compose([Validators.required])],
      level:[question.level,Validators.compose([Validators.required])],
      fact:[question.fact,Validators.compose([Validators.required])],
      wrong_option: question.wrong_option
    })


    if(!question.options){
      this.generateOptions(null,this.appUiService.defaultOptionsCount)
    }else{
      this.generateOptions(question.options,question.options.length)
      setTimeout( this.selectFieldsOnEdit,500,this);
    }


  }



  loadQuestions(){

    if(this.showArchived){
      this.questionsService.getArchivedQuestions().subscribe(
        (res)=>{
          console.log(res)
          this.temp = _clone(res['data']);
          this.rows = _clone(res['data']);
          this.rowsFilter = _clone(res['data']);

          // Setting App Data
          this.appDataService.questions = res['data'];
        }
      )
    }else{
      this.questionsService.getQuestions().subscribe(
        (res)=>{
          console.log(res)
          this.temp = _clone(res['data']);
          this.rows = _clone(res['data']);
          this.rowsFilter = _clone(res['data']);

          // Setting App Data
          this.appDataService.questions = res['data'];
        }
      )
    }
  }


  updateFilter(event) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function(d) {
      return d.question.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.rowsFilter = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  // Selection


  filterBy(field,value){
    value = value.toLowerCase();
    // filter our data
    const temp = this.temp.filter(function(d) {

      var flds =  field.split('.');
      var dfld = null;

      for(var i=0;i<flds.length;i++){
        dfld = (dfld)?dfld = dfld[flds[i]]:d[flds[i]];
      }

      return dfld.toLowerCase().indexOf(value) !== -1 || !value;
    });

    // update the rows
    this.rowsFilter = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }




  onSelect({ selected }) {
    this.selected = selected
  }



  resetAll() {
    this.closeQuestionModal();
    this.loadQuestions();
    this.selected = [];
    this.questionForm.reset();

    this.currentSelection = null;

    this.questionForm.value.type = Q_TYPE_SINGLE;
    (<HTMLFormElement>this.questionFormDom).elements[Q_TYPE_SINGLE].checked = true;

    (<HTMLFormElement>this.questionFormDom).elements['category'].selectedIndex = 0;
    (<HTMLFormElement>this.questionFormDom).elements['level'].selectedIndex = 0;

    this.payLoad = '';
    this.isCorrectSelectionError = false;

    this.appDataService.refreshAll();

  }


  createOrModifyQuestion(){

    this.isCorrectSelectionError = false;

    // TODO:  Logic to set correct answer, Works for now but find batter approach.
    var opts = this.questionForm.value['options'];

    var correct_answer = [];

    for(var i=0;i<opts.length;i++){
        if(this.questionForm.value.type == Q_TYPE_SEQUENCE){
          opts[i].isCorrect = true;
          correct_answer.push(opts[i].title)
        }else{
          opts[i].isCorrect = (<HTMLFormElement>this.questionFormDom).elements['option-'+i].checked;
          if(opts[i].isCorrect){
            correct_answer.push(opts[i].title);
          }
        }
    }
    this.questionForm.value.options = opts;
    ///////////////////////////////////////////////////////////////////////////




    var question = this.questionForm.value;

    question.correct_answer = correct_answer.toString();

    if(correct_answer.toString() == ""){
      this.isCorrectSelectionError = true;
      return;
    }




    this.payLoad = JSON.stringify(question);



    if(this.questionMode == Q_UI_ADD){
      this.questionsService.createQuestion(question).subscribe(
        (res)=>{
          console.log(res)
          this.appUiService.showSuccessTypeToast(QUESTION,CREATED);
          this.resetAll();
        }
      )
    }else{
      this.questionsService.modifyQuestion(question,this.currentSelection.question_id).subscribe(
        (res)=>{
          console.log(res)
          this.appUiService.showSuccessTypeToast(QUESTION,UPDATED);
          this.resetAll();
        }

      )
    }




  }







  deleteQuestion(question){
    this.questionsService.deleteQuestion(question.question_id).subscribe(
      (res)=>{
        console.log(res)
        this.appUiService.showSuccessTypeToast(CATEGORY,DELETED);
        this.resetAll();
      }
    )

  }




  ///////////// Add Dynamic Options Field /////////


  addOption(isCorrect,title,oid){

    var optionFg:FormGroup =  new FormGroup({'isCorrect': new FormControl(isCorrect),  'title': new FormControl(title), 'oid': new FormControl(oid)});
    optionFg.controls.title.validator = Validators.required;

    this.addQuestionFromOptions.push(optionFg)
  }


  generateOptions(options,count){

    for(var i=0;i<count; i++){

      if(!options){
        this.addOption(false,"",uuid.v4());
      }else{
        this.addOption(options[i].isCorrect,options[i].title,options[i].oid);
      }
    }

  }


  selectFieldsOnEdit(this$){
    var options = this$.questionForm.value.options;
    if(options){
      for(var i=0;i<options.length;i++){
        if(this$.questionForm.value.type != Q_TYPE_SEQUENCE){
          this$.questionFormDom.elements['option-'+i].checked = options[i].isCorrect;
        }
      }
    }

    this$.selectDropDown(this$.questionFormDom.elements['category'],this$.questionForm.value.category);
    this$.selectDropDown(this$.questionFormDom.elements['level'],this$.questionForm.value.level);
    this$.selectDropDown(this$.questionFormDom.elements['wrong_option'],this$.questionForm.value.wrong_option);

  }

  selectDropDown(dropdown,value){
    for(var i=0;i<dropdown.options.length;i++){
      if(dropdown.options[i].value == value){
        dropdown.selectedIndex = i;
        break;
      }
    }
  }

  ///////////////////



  ngAfterViewInit(){
    this.questionFormDom = document.getElementById('questionForm');
  }



  addNewQuestion(){
    this.questionMode = Q_UI_ADD;
    this.resetAll();
    this.initQuestionForm(null);
    this.questionModal.show()
  }

  closeQuestionModal()
  {
    this.questionModal.hide()
  }



  removeQuestion(question){
    SweetAlert({
      title: 'Do you want to DELETE following question?',
      text: question.question,
      type: 'error',
      showCancelButton: true,
      confirmButtonColor: '#DD6B55',
      confirmButtonText: 'Yes, delete it!',
      closeOnConfirm: true
    }, () => {
      this.deleteQuestion(question);
    });

  }

  editQuestion(question){

    var que = Object.assign({}, question);
    (question.category) ? que.category = question.category._id : que.category = '';
    (question.level) ? que.level = question.level._id : question.level = '';

    this.currentSelection = question;

    this.questionMode = Q_UI_EDIT;
    this.initQuestionForm(que);
    this.questionModal.show();

  }



}
