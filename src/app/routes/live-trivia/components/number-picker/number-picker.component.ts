import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'number-picker',
  templateUrl: './number-picker.component.html',
  styleUrls: ['./number-picker.component.scss']
})
export class NumberPickerComponent implements OnInit {
	@Input() min: number;
	@Input() max: number;
	@Input() step: number;
  @Input() value: number;
	@Input() precision: number;
	@Input() inputDisabled: boolean;
	@Output() onChange: EventEmitter<number> = new EventEmitter();
  @Input() size:number = 3;

	private numberPicker: FormControl;

	constructor() {}

	ngOnInit() {
		if(this.inputDisabled == null) {
			this.inputDisabled = false;
		}
    if(this.min == null) {
      this.min = 0;
    }
    if(this.max == null) {
      this.max = 100;
    }
    if(this.precision == null) {
      this.precision = 1;
    }
    if(this.step == null) {
      this.step = 1;
    }
    if(this.value == null) {
      this.value = this.min;
    }

		this.numberPicker = new FormControl({value: this.value, disabled: this.inputDisabled});
		this.numberPicker.registerOnChange(() => {
			this.onChange.emit(this.numberPicker.value);
		});
  	}

  	private increaseValue(): void{
  		var currentValue = this.numberPicker.value;
  		if(currentValue < this.max) {
  			currentValue = currentValue+this.step;
  			if(this.precision != null) {
  				currentValue = this.round(currentValue, this.precision);
  			}
  			this.numberPicker.setValue(currentValue);
  			this.value = currentValue;
  		}
  	}

  	private decreaseValue(): void {
  		var currentValue = this.numberPicker.value;
  		if(currentValue > this.min) {
  			currentValue = currentValue-this.step;
  			if(this.precision != null) {
  				currentValue = this.round(currentValue, this.precision);
  			}
  			this.numberPicker.setValue(currentValue);
        this.value = currentValue;
      }
  	}

  	private round(value:number, precision:number): number {
  		let multiplier : number = Math.pow(10, precision || 0);
    	return Math.round(value * multiplier) / multiplier;
  	}

  	public getValue(): number {
  		return this.numberPicker.value;
  	}

    public setValue(val) {
      this.numberPicker.setValue(val);
      this.value = val;
    }

}
