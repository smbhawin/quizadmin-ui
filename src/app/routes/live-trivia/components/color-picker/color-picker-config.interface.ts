export interface IColorPickerConfiguration {
  width?: number;
  height?: number;
  borderRadius?: number;
  marginToCenter?: number;
}
