import { Component, Input, Output, EventEmitter, OnInit, Self } from '@angular/core';
import { NgModel, ControlValueAccessor } from '@angular/forms';
import {IColorPickerConfiguration} from "./color-picker-config.interface";
import {ColorPickerConfiguration} from "./color-picker-config.model";


@Component({
  selector: 'color-picker',
  templateUrl: 'color-picker.component.html',
  styleUrls: ['color-picker.component.scss'],
  providers: [ NgModel ]
})
export class ColorPickerComponent implements ControlValueAccessor, OnInit {

  public cd: NgModel;

  public onChange: any = Function.prototype;

  public onTouched: any = Function.prototype;

  public config: ColorPickerConfiguration;

  @Input() pickerConfig: IColorPickerConfiguration;

  @Input() availableColors: string[];

  constructor(@Self() cd: NgModel) {
    this.cd = cd;
    cd.valueAccessor = this;

    this.config = new ColorPickerConfiguration();
  }

  /** OnInit implementation. */
  ngOnInit() {
    this._processOptions(this.pickerConfig);
  }

  /** ControlValueAccessor implementation. */
  writeValue(value: any): void { }

  /** ControlValueAccessor implementation. */
  registerOnChange(fn: (_: any) => {}): void {
    this.onChange = fn;
  }

  /** ControlValueAccessor implementation. */
  registerOnTouched(fn: (_: any) => {}): void {
    this.onTouched = fn;
  }

  /**
   * Update the current color based on selection.
   *
   * @param {string} color
   *
   * @memberOf ColorPickerComponent
   */
  public setColor(color: string) {
    this.cd.viewToModelUpdate(color);
    this.onTouched();
  }

  /**
   * Wire up configuration.
   *
   * @private
   * @param {IColorPickerConfiguration} opts
   *
   * @memberOf ColorPickerComponent
   */
  private _processOptions(opts: IColorPickerConfiguration) {
    if (opts != null) {

      const IsNumber = (val: any) => typeof(val) === 'number';
      const IsArray = (val: any) => Array.isArray(val);

      // width
      if (IsNumber(opts.width)) {
        this.config.width = opts.width;
      }

      // height
      if (IsNumber(opts.height)) {
        this.config.height = opts.height;
      }

      // borderRadius
      if (IsNumber(opts.borderRadius)) {
        this.config.borderRadius = opts.borderRadius;
      }
    }

    if (this.availableColors == null) {
      console.warn('[ng2-color-picker] No available colors provided.');
    }
  }
}
