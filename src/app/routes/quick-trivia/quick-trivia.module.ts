import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from '../../shared/shared.module';
import { UsersComponent } from './users/users.component';
import { LevelsComponent } from './levels/levels.component';
import { CategoriesComponent } from './categories/categories.component';
import { QuestionsComponent } from './questions/questions.component';
import { GamesComponent } from './games/games.component';
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {ColorPickerComponent} from "./components/color-picker/color-picker.component";
import {OwlDateTimeModule, OwlNativeDateTimeModule} from "ng-pick-datetime";
import {NumberPickerComponent} from "./components/number-picker/number-picker.component";
const routes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'levels', component: LevelsComponent },
  { path: 'categories', component: CategoriesComponent },
  { path: 'questions', component: QuestionsComponent },
  { path: 'games', component: GamesComponent },
];

@NgModule({
  imports: [
    SharedModule,
    NgxDatatableModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    UsersComponent,
    LevelsComponent,
    CategoriesComponent,
    QuestionsComponent,
    GamesComponent,
    ColorPickerComponent,
    NumberPickerComponent
  ],
  exports: [
    RouterModule
  ],
})
export class QuickTriviaModule { }
