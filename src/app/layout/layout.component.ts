import { Component, OnInit } from '@angular/core';
import {AppUiService} from "../services/app-ui.service";
import {ToasterService} from "angular2-toaster";

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    constructor(public appUiService:AppUiService, public toastService: ToasterService) {
      appUiService.toastService = toastService;
    }

    ngOnInit() {
    }

}
